# Classification of Cortex and Hippocampus Cell Subtypes
## Background

Single cell RNAseq has become widely used method for cell subtypes 
identification. It is based on finding the level of gene expression in hundreds 
(thousands) of single cells obtained from specific tissue. Subsequent clustering
then allows for cell subtypes indentification. Further, the activity of specific 
biological pathways connected to specific cell types is usually explored. 

## Project goal

Goal of this project is to create an efficient nerual network classifier of mouse brain cells, which will be able to identify cell subtype based on expression of specific genes.

## Input Data
For this project, [data](https://storage.googleapis.com/linnarsson-lab-www-blobs/blobs/cortex/expression_mRNA_17-Aug-2014.txt) 
from [study](https://doi.org/10.1126/science.aaa1934) performed on mouse hippocampus
and cortex cells will be used. 
Input data consist of 3006 cells divided into 9 clusters (each represents one cell subtype). For each cell, the 
information about expression level of 19970 genes is provided. 

